def while_simple():
    i = 0
    while i < 10:
        i += 1
        print("we're on iteration:", i)
        

def while_condition():
    i = 0
    while i < 10:
        if (i % 2) == 0:
            print("we're on iteration:", i)
        i += 1


def for_simple():
    n = int(input("how many times would you like to loop? "))
    for i in range(1, n+1):
        print("we're on iteration:", i)


def for_condition():
    for i in range(0, 10, 2):
        if (i % 2) == 0:
            print("we're on iteration:", i)
        else:
            print("not even")


#while_simple()
#while_condition()
#for_simple()
for_condition()



