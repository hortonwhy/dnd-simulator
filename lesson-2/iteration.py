us = ["galvin", "shawn", "luciano", "ratmoney"]

def simple_loop():
    for person in us:
        print(person, end="\n")


def while_len():
    length = len(us)
    i = 0
    while i < length:
        print(us[i])
        i+= 1


#simple_loop()
while_len()


